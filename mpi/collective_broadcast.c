#include<stdio.h>
#include <stdlib.h>
#include <time.h>
#include<mpi.h>

int random_between(int min, int max) {
    return (int) (rand() % (max - min) + min);
}

int main(int argc, char** argv) {
    srand(time(NULL));

    int rank, random_value, root = 0;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == root) {
        random_value = random_between(1, 10);
    }

    MPI_Bcast(&random_value, 1, MPI_INT, root, MPI_COMM_WORLD);

    printf("[%d] - Random value is %d\n", rank, random_value);

    MPI_Finalize();
    return 0;
}
