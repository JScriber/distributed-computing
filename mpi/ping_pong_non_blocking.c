#include<stdio.h>
#include<mpi.h>

int main(int argc, char** argv) {
    int rank, tag = 42, datas_1 = 0, datas_2 = 0, datar_1 = 0, datar_2 = 0;
    MPI_Request send_request_1, send_request_2, recv_request_1, recv_request_2;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 0) {
        // Wait the very last moment to set the send buffer.
        datas_1 = 1337;
        datas_2 = 10;

        MPI_Isend(&datas_1, 1, MPI_INT, 1, tag, MPI_COMM_WORLD, &send_request_1);
        MPI_Isend(&datas_2, 1, MPI_INT, 1, tag, MPI_COMM_WORLD, &send_request_2);

        MPI_Irecv(&datar_1, 1, MPI_INT, 1, tag, MPI_COMM_WORLD, &recv_request_1);
        MPI_Irecv(&datar_2, 1, MPI_INT, 1, tag, MPI_COMM_WORLD, &recv_request_2);

        MPI_Wait(&recv_request_1, MPI_STATUS_IGNORE);
        MPI_Wait(&recv_request_2, MPI_STATUS_IGNORE);

        printf("[%d] - S: %d | R: %d\n", rank, datas_1, datar_1);
        printf("[%d] - S: %d | R: %d\n", rank, datas_2, datar_2);
    } else {
        MPI_Irecv(&datar_1, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &recv_request_1);
        MPI_Irecv(&datar_2, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &recv_request_2);

        MPI_Wait(&recv_request_1, MPI_STATUS_IGNORE);
        MPI_Wait(&recv_request_2, MPI_STATUS_IGNORE);

        printf("[%d] - S: %d | R: %d\n", rank, datas_1, datar_1);
        printf("[%d] - S: %d | R: %d\n", rank, datas_2, datar_2);

        MPI_Isend(&datar_1, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &send_request_1);
        MPI_Isend(&datar_2, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &send_request_2);
    }

    MPI_Finalize();
    return 0;
}
