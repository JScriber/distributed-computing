#include<stdio.h>
#include<mpi.h>

int main(int argc, char** argv) {
    int rank, tag = 42, datas = 0, datar = 0;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 0) {
        // Wait the very last moment to set the send buffer.
        datas = 1337;

        printf("[%d] - BEFORE - S: %d | R: %d\n", rank, datas, datar);

        MPI_Send(&datas, 1, MPI_INT, 1, tag, MPI_COMM_WORLD);
        MPI_Recv(&datar, 1, MPI_INT, 1, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        printf("[%d] - AFTER - S: %d | R: %d\n", rank, datas, datar);
    } else {
        printf("[%d] - BEFORE - S: %d | R: %d\n", rank, datas, datar);
        MPI_Recv(&datar, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("[%d] - AFTER - S: %d | R: %d\n", rank, datas, datar);
        MPI_Send(&datar, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
    }

    MPI_Finalize();
    return 0;
}
