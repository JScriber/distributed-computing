#include<stdio.h>
#include<mpi.h>

int get_previous_rank(int rank, int size) {
    int previous_rank = rank - 1;
    if (previous_rank < 0) {
        previous_rank = size - 1;
    }
    return previous_rank;
}

int get_next_rank(int rank, int size) {
    int next_rank = rank + 1;
    if (next_rank >= size) {
        next_rank = 0;
    }
    return next_rank;
}

int main(int argc, char** argv) {
    int rank, size, tag = 42, buffer = 0;
    int ring_initiator = 0, total_laps = 2, lap = 0;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Initiator.
    if (rank == ring_initiator) {
        buffer = 20;
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        int next_rank = get_next_rank(rank, size);

        printf("[%d] - Ring started for %d laps\n", rank, total_laps);
        MPI_Send(&buffer, 1, MPI_INT, next_rank, tag, MPI_COMM_WORLD);
    }

    while (lap < total_laps) {

        // Receive from k - 1.
        int previous_rank = get_previous_rank(rank, size);
        MPI_Recv(&buffer, 1, MPI_INT, previous_rank, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("[%d] - Has received %d\n", rank, buffer);

        lap ++;

        if (rank == ring_initiator && lap == total_laps) {
            printf("[%d] - Ring is closed.\n", rank);
        } else {
            // Send to k + 1
            int next_rank = get_next_rank(rank, size);
            printf("[%d] - Send to %d\n", rank, next_rank);
            MPI_Send(&buffer, 1, MPI_INT, next_rank, tag, MPI_COMM_WORLD);
        }
    }

    // End program.
    MPI_Finalize();
    return 0;
}
