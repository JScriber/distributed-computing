#include<stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>

/**
 * Raises each value in the array to its square value.
 * @param array
 * @param size of the array.
 */
void raise_to_square(float *array, int size) {
    for (int i = 0; i < size; i ++) {
        float* array_addr = array + i;
        float value = *array_addr;

        (*array_addr) = value * value;
    }
}

/**
 * Calculates the average of the array of numbers.
 * @param array
 * @param size
 * @return average
 */
float calculate_average(float* array, int size) {
    int total = 0;
    for (int i = 0; i < size; i++){
        total += array[i];
    }

    return total / (float) size;
}

int main(int argc, char** argv) {
    int rank, size, root = 0;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (size % 2 == 0) {
        // Init array.
        int array_size = 8;
        float array[8] = {1, 2, 3, 4, 5, 6, 7, 8};
        int count = array_size / size;

        float receiver[count];

        MPI_Scatter(&array, count, MPI_INT, &receiver, count, MPI_INT, root, MPI_COMM_WORLD);

        // Calculation in each process.
        raise_to_square(receiver, count);
        float average_process = calculate_average(receiver, count);

        // Total sum.
        float averages[size];
        MPI_Gather(&average_process, 1, MPI_INT, &averages, 1, MPI_INT, root, MPI_COMM_WORLD);

        if (rank == root) {
            float average = calculate_average(averages, size);

            printf("[%d] - Average: %f\n", rank, average);
        }
    } else {
        printf("Need an even number of cores.\n");
    }

    MPI_Finalize();
    return 0;
}
