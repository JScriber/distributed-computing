#include<stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>
#include <unistd.h>

int random_between(int min, int max) {
    return (int) (rand() % (max - min) + min);
}

int main(int argc, char** argv) {
    srand(getpid());

    int rank, root = 0, sender, receiver;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    sender = random_between(1, 100);

    printf("[%d] - Add %d\n", rank, sender);

    MPI_Reduce(&sender, &receiver, 1, MPI_INT, MPI_SUM, root, MPI_COMM_WORLD);

    if (rank == root) {
        printf("[%d] - Sum is %d\n", rank, receiver);
    }

    MPI_Finalize();
    return 0;
}
