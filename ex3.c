#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

struct ThreadArg {
    int id;
    int output;
};

long value = 0;


int random_between(int seed, int min, int max) {
    srand(seed);
    return (int) (rand() % (max - min) + min);
}

void* thread_executor(void* arg) {
    struct ThreadArg* thread_arg = (struct ThreadArg *) arg;

    printf("[%d] - Started\n", thread_arg->id);

    for (int i = 0; i < 1000000; i ++) {
        value++;
    }

    printf("[%d] - Ended.\n", thread_arg->id);
    pthread_exit(0);
}

int main(void) {
    int total_thread = 10;
    pthread_t threads[total_thread];
    struct ThreadArg arguments[total_thread];

    // Start parallel threads.
    for (int i = 0; i < total_thread; i ++) {
        struct ThreadArg thread_arg;
        thread_arg.id = i;
        arguments[i] = thread_arg;

        int status_code = pthread_create(&threads[i], NULL, thread_executor, &arguments[i]);

        if (status_code != 0) {
            printf("Error while creating %d.\n", i);
        }
    }

    // Wait completion of all threads.
    for (int i = 0; i < total_thread; i ++) {
        int status_code = pthread_join(threads[i], NULL);

        if (status_code != 0) {
            printf("Error while waiting %d.\n", i);
        }
    }

    printf("Final value is %ld", value);

    return 0;
}
