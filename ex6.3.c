#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

struct ThreadArg {
    int id;
    pthread_spinlock_t* spinlock;
};

long value = 0;

void sub_timespec(struct timespec t1, struct timespec t2, struct timespec *td)
{
    td->tv_nsec = t2.tv_nsec - t1.tv_nsec;
    td->tv_sec  = t2.tv_sec - t1.tv_sec;
    if (td->tv_sec > 0 && td->tv_nsec < 0)
    {
        td->tv_nsec += 1000000000;
        td->tv_sec--;
    }
    else if (td->tv_sec < 0 && td->tv_nsec > 0)
    {
        td->tv_nsec -= 1000000000;
        td->tv_sec++;
    }
}

int random_between(int seed, int min, int max) {
    srand(seed);
    return (int) (rand() % (max - min) + min);
}

void* thread_executor(void* arg) {
    struct ThreadArg* thread_arg = (struct ThreadArg *) arg;

    pthread_spin_lock(thread_arg->spinlock);
    printf("[%d] - Started\n", thread_arg->id);

    for (int i = 0; i < 1000000000; i ++) {
        value++;
    }

    printf("[%d] - Ended.\n", thread_arg->id);
    pthread_spin_unlock(thread_arg->spinlock);
    pthread_exit(0);
}

int main(void) {
    int total_thread = 4;
    pthread_t threads[total_thread];
    struct ThreadArg arguments[total_thread];

    pthread_spinlock_t spinlock;
    pthread_spin_init(&spinlock, 0);

    // Start parallel threads.
    for (int i = 0; i < total_thread; i ++) {
        struct ThreadArg thread_arg;
        thread_arg.id = i;
        thread_arg.spinlock = &spinlock;
        arguments[i] = thread_arg;

        int status_code = pthread_create(&threads[i], NULL, thread_executor, &arguments[i]);

        if (status_code != 0) {
            printf("Error while creating %d.\n", i);
        }
    }

    // Wait completion of all threads.
    struct timespec begin_process, end_process, time_process;
    clock_gettime(CLOCK_MONOTONIC, &begin_process);

    for (int i = 0; i < total_thread; i ++) {
        int status_code = pthread_join(threads[i], NULL);

        if (status_code != 0) {
            printf("Error while waiting %d.\n", i);
        }
    }

    clock_gettime(CLOCK_MONOTONIC, &end_process);
    sub_timespec(begin_process, end_process, &time_process);

    printf("[main] - Final value is %ld\n", value);
    printf("[main] - Time process: %d.%.9ld\n", (int)time_process.tv_sec, time_process.tv_nsec);
    pthread_spin_destroy(&spinlock);

    return 0;
}
