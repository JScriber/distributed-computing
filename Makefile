ex1.o: ex1.c
	gcc -pedantic -Wall -Wextra ex1.c -lpthread -o ex1.o

ex2.o: ex2.c
	gcc -pedantic -Wall -Wextra ex2.c -lpthread -o ex2.o

ex3.o: ex3.c
	gcc -pedantic -Wall -Wextra ex3.c -lpthread -o ex3.o

ex4.o: ex4.c
	gcc -pedantic -Wall -Wextra ex4.c -lpthread -lm -o ex4.o

ex5.o: ex5.c
	gcc -pedantic -Wall -Wextra ex5.c -lpthread -lm -o ex5.o

ex5.6.o: ex5.6.c
	gcc -pedantic -Wall -Wextra ex5.6.c -lpthread -lm -o ex5.6.o

ex6.o: ex6.c
	gcc -pedantic -Wall -Wextra ex6.c -lpthread -lm -o ex6.o

ex6.3.o: ex6.3.c
	gcc -pedantic -Wall -Wextra ex6.3.c -lpthread -lm -o ex6.3.o

mpi-hello-world: mpi/hello_world.c
	mpicc -pedantic -Wall -Wextra -o hello.o mpi/hello_world.c

mpi-ping-pong: mpi/ping_pong.c
	mpicc -pedantic -Wall -Wextra -o ping_pong.o mpi/ping_pong.c

mpi-ring: mpi/ring.c
	mpicc -pedantic -Wall -Wextra -o ring.o mpi/ring.c

mpi-ping-pong-non-blocking: mpi/ping_pong_non_blocking.c
	mpicc -pedantic -Wall -Wextra -o ping_pong_non_blocking.o mpi/ping_pong_non_blocking.c

mpi-collective-broadcast: mpi/collective_broadcast.c
	mpicc -pedantic -Wall -Wextra -o collective_broadcast.o mpi/collective_broadcast.c

mpi-collective-broadcast-non-blocking: mpi/collective_broadcast_non_blocking.c
	mpicc -pedantic -Wall -Wextra -o collective_broadcast_non_blocking.o mpi/collective_broadcast_non_blocking.c

mpi-collective-reduce: mpi/collective_reduce.c
	mpicc -pedantic -Wall -Wextra -o collective_reduce.o mpi/collective_reduce.c

mpi-collective-scatter: mpi/collective_scatter.c
	mpicc -pedantic -Wall -Wextra -o collective_scatter.o mpi/collective_scatter.c
