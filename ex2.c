#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

struct ThreadArg {
    int id;
    int output;
};


int random_between(int seed, int min, int max) {
    srand(seed);
    return (int) (rand() % (max - min) + min);
}

void* thread_executor(void* arg) {
    struct ThreadArg* thread_arg = (struct ThreadArg *) arg;
    int time = random_between(thread_arg->id, 1, 5);

    printf("[%d] - Started (wait %d)\n", thread_arg->id, time);
    sleep(time);

    printf("[%d] - Wait 10s\n", thread_arg->id);
    sleep(10);

    thread_arg->output = thread_arg->id * thread_arg->id;

    printf("[%d] - Ended.\n", thread_arg->id);
    pthread_exit(0);
}

int main(void) {
    int total_thread = 10;
    pthread_t threads[total_thread];
    struct ThreadArg arguments[total_thread];

    // Start parallel threads.
    for (int i = 0; i < total_thread; i ++) {
        struct ThreadArg thread_arg;
        thread_arg.id = i;
        arguments[i] = thread_arg;

        int status_code = pthread_create(&threads[i], NULL, thread_executor, &arguments[i]);

        if (status_code != 0) {
            printf("Error while creating %d.\n", i);
        }
    }

    // Wait completion of all threads.
    for (int i = 0; i < total_thread; i ++) {
        int status_code = pthread_join(threads[i], NULL);
        struct ThreadArg thread_arg = arguments[i];

        printf("Computed value for %d is %d\n", thread_arg.id, thread_arg.output);

        if (status_code != 0) {
            printf("Error while waiting %d.\n", i);
        }
    }

    return 0;
}
