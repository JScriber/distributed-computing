#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

void* thread_executor() {
    printf("Hello World.\n");
    sleep(10);
    printf("Bye.\n");

    pthread_exit(0);
}

int main(void) {
    int total_thread = 3;
    pthread_t threads[total_thread];

    // Start parallel threads.
    for (int i = 0; i < total_thread; i ++) {
        int status_code = pthread_create(&threads[i], NULL, thread_executor, NULL);

        if (status_code != 0) {
            printf("Error while creating %d.\n", i);
        }
    }

    // Wait completion of all threads.
    for (int i = 0; i < total_thread; i ++) {
        int status_code = pthread_join(threads[i], NULL);

        if (status_code != 0) {
            printf("Error while waiting %d.\n", i);
        }
    }

    return 0;
}
