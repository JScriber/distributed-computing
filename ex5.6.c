#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <math.h>

pthread_barrier_t barrier;

struct ThreadArg {
    int id;
    int sum;
    int* array;
    int array_size;
    int step;
};

unsigned long time_spent(clock_t* begin, clock_t* end) {
    return (*end - *begin) * 1000 / CLOCKS_PER_SEC;
}

int random_between(int min, int max) {
    return (int) (rand() % (max - min) + min);
}

void* thread_executor(void* arg) {
    pthread_barrier_wait(&barrier);

    struct ThreadArg* thread_arg = (struct ThreadArg *) arg;
    int sum = 0;

    for (int i = thread_arg->id; i < thread_arg->array_size; i += thread_arg->step) {
        int value = thread_arg->array[i];
        sum += value;
    }

    thread_arg->sum = sum;
    pthread_exit(0);
}

int main(void) {
    srand(time(NULL));

    int total_thread = 4;
    pthread_t threads[total_thread];
    struct ThreadArg arguments[total_thread];

    // Create random array.
//    int random_size = random_between(500, 1000);
    int random_size = 1000000000;
    int* array = malloc(sizeof(int) * random_size);

    for (int i = 0; i < random_size; i++) {
//        array[i] = random_between(0, 9);
        array[i] = 1;
    }

    printf("[main] - Array size: %d\n", random_size);
    printf("[main] - Threads: %d\n", total_thread);

    // Create the barrier.
    clock_t begin_setup = clock();
    pthread_barrier_init(&barrier, NULL, total_thread + 1);

    for (int i = 0; i < total_thread; i ++) {
        struct ThreadArg thread_arg;
        thread_arg.id = i;
        thread_arg.array = array;
        thread_arg.array_size = random_size;
        thread_arg.step = total_thread;

        arguments[i] = thread_arg;

        int status_code = pthread_create(&threads[i], NULL, thread_executor, &arguments[i]);

        if (status_code != 0) {
            printf("Error while creating %d.\n", i);
        }
    }

    pthread_barrier_wait(&barrier);
    clock_t end_setup = clock();
    clock_t begin_process = clock();

    // Wait completion of all threads.
    long sum = 0;

    for (int i = 0; i < total_thread; i ++) {
        int status_code = pthread_join(threads[i], NULL);
        sum += arguments[i].sum;

        if (status_code != 0) {
            printf("Error while waiting %d.\n", i);
        }
    }

    pthread_barrier_destroy(&barrier);
    clock_t end_process = clock();

    printf("[main] - Sum: %ld.\n", sum);
    printf("[main] - Time setup: %lu ms\n", time_spent(&begin_setup, &end_setup));
    printf("[main] - Time process: %lu ms\n", time_spent(&begin_process, &end_process));

    return 0;
}
