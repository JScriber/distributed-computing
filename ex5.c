#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

struct ThreadArg {
    int id;
    unsigned long time;
};

void* thread_executor(void* arg) {
    struct ThreadArg* thread_arg = (struct ThreadArg *) arg;
    clock_t begin = clock();
    sleep(1);
    clock_t end = clock();

    unsigned long time = (end - begin) * 100 / CLOCKS_PER_SEC;
    thread_arg->time = time;

    pthread_exit(0);
}

int main(void) {
    int total_thread = 100;
    pthread_t threads[total_thread];
    struct ThreadArg arguments[total_thread];

    // Start parallel threads.
    for (int i = 0; i < total_thread; i ++) {
        struct ThreadArg thread_arg;
        thread_arg.id = i;
        arguments[i] = thread_arg;

        int status_code = pthread_create(&threads[i], NULL, thread_executor, &arguments[i]);

        if (status_code != 0) {
            printf("Error while creating %d.\n", i);
        }
    }

    // Wait completion of all threads.
    unsigned long total = 0;

    for (int i = 0; i < total_thread; i ++) {
        int status_code = pthread_join(threads[i], NULL);
        unsigned long time = arguments[i].time;
        total += time;

        printf( "[%d] - Finished in %lu ds.\n", i, time);

        if (status_code != 0) {
            printf("Error while waiting %d.\n", i);
        }
    }

    unsigned long average_time = total / total_thread;
    printf("[main] - Average time: %lu ds\n", average_time);

    return 0;
}
