#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <math.h>

pthread_barrier_t barrier;

struct ThreadArg {
    int id;
    int sum;
    int* array;
    int start;
    int end;
};

void sub_timespec(struct timespec t1, struct timespec t2, struct timespec *td)
{
    td->tv_nsec = t2.tv_nsec - t1.tv_nsec;
    td->tv_sec  = t2.tv_sec - t1.tv_sec;
    if (td->tv_sec > 0 && td->tv_nsec < 0)
    {
        td->tv_nsec += 1000000000;
        td->tv_sec--;
    }
    else if (td->tv_sec < 0 && td->tv_nsec > 0)
    {
        td->tv_nsec -= 1000000000;
        td->tv_sec++;
    }
}

int random_between(int min, int max) {
    return (int) (rand() % (max - min) + min);
}

void* thread_executor(void* arg) {
    pthread_barrier_wait(&barrier);
    struct ThreadArg* thread_arg = (struct ThreadArg *) arg;
    int seg_size = thread_arg->end - thread_arg->start;
    int sum = 0;

    for (int i = 0; i <= seg_size; i++) {
        int value = thread_arg->array[thread_arg->start + i];
        sum += value;
//        printf("[%d] - %d\n", thread_arg->id, value);
    }

    thread_arg->sum = sum;

    pthread_exit(0);
}

int main(void) {
    srand(time(NULL));

    int total_thread = 2;
    pthread_t threads[total_thread];
    struct ThreadArg arguments[total_thread];

    // Create random array.

//    int random_size = random_between(500, 1000);
    int random_size = 1000000000;
    int* array = malloc(sizeof(int) * random_size);

    for (int i = 0; i < random_size; i++) {
//        array[i] = random_between(0, 9);
        array[i] = 1;
    }

    int seg_size = ceil((double) random_size / total_thread);
    int used_threads = ceil((double) random_size / seg_size);

    printf("[main] - Array size: %d\n", random_size);
    printf("[main] - Threads usage: %d/%d\n", used_threads, total_thread);
    printf("[main] - Segment size: %d\n", seg_size);

    // Create the barrier.
    struct timespec begin_setup, end_setup, time_setup, begin_process, end_process, time_process;

    clock_gettime(CLOCK_MONOTONIC, &begin_setup);
    pthread_barrier_init(&barrier, NULL, used_threads + 1);

    for (int i = 0; i < used_threads; i ++) {
        int start = i * seg_size;
        int end = (i * seg_size) + (seg_size - 1);

        if (end > (random_size - 1)) {
            end = random_size - 1;
        }

        if (start < random_size) {
            struct ThreadArg thread_arg;
            thread_arg.id = i;
            thread_arg.array = array;
            thread_arg.start = start;
            thread_arg.end = end;

            arguments[i] = thread_arg;

            int status_code = pthread_create(&threads[i], NULL, thread_executor, &arguments[i]);

            if (status_code != 0) {
                printf("Error while creating %d.\n", i);
            }
        }
    }

    pthread_barrier_wait(&barrier);
    clock_gettime(CLOCK_MONOTONIC, &end_setup);
    sub_timespec(begin_setup, end_setup, &time_setup);

    clock_gettime(CLOCK_MONOTONIC, &begin_process);
    // Wait completion of all threads.
    long sum = 0;

    for (int i = 0; i < used_threads; i ++) {
        int status_code = pthread_join(threads[i], NULL);
        sum += arguments[i].sum;

        if (status_code != 0) {
            printf("Error while waiting %d.\n", i);
        }
    }

    pthread_barrier_destroy(&barrier);
    clock_gettime(CLOCK_MONOTONIC, &end_process);
    sub_timespec(begin_process, end_process, &time_process);

    printf("[main] - Sum: %ld.\n", sum);
    printf("[main] - Time setup: %d.%.9ld\n", (int)time_setup.tv_sec, time_setup.tv_nsec);
    printf("[main] - Time process: %d.%.9ld\n", (int)time_process.tv_sec, time_process.tv_nsec);

    return 0;
}
